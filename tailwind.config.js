module.exports = {
  /* Purge will remove other unused classes */
  purge: {
    enabled : process.env.NODE_ENV === 'production' ? true : false,
    /* mention the directory in which css exists */
    /* it will look on on those dir */
    content : [
      "./theme/sections/*.liquid",
      "./theme/layout/*.liquid",
      "./theme/templates/*.liquid",
      "./theme/snippets/*.liquid"
    ]
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
