  $(function() {
    var currentTags =simply.collectionCurrentTags,
    filters = $('.advanced-filter'),
      activeTag,
      activeHandle;

    filters.each(function() {
      var el = $(this),
          group = el.data('group'),
          isActive = el.hasClass('active-filter');
    });

    filters.on('click', function(e) {
      var el = $(this),
          group = el.data('group'),
          url = el.find('a').attr('href');

      // Continue as normal if we're clicking on the active link
      if ( el.hasClass('active-filter') ) {
        return;
      }

      // Get active group link (unidentified if there isn't one)
      activeTag = $('.active-filter[data-group="'+ group +'"]');

      // If a tag from this group is already selected, remove it from the new tag's URL and continue
      if ( activeTag && activeTag.data('group') === group ) {
        e.preventDefault();
        activeHandle = activeTag.data('handle') + '+';

        // Create new URL without the currently active handle
        url = url.replace(activeHandle, '');

        window.location = url;
      }
    });
  });
