class FeaturedProduct {
  constructor(selector) {
    this.selector = selector;
    this.settings;
  }

  setSettings = () => {
    let parent = this.selector.closest(".featured-products");
    settings = {
      slidesToShow: 4,
      slidesToScroll: 1,
      prevArrow: $(".arrow-left", parent),
      nextArrow: $(".arrow-right", parent),
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows: false,
            dots: true
          }
        },
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        }
      ]
    }
    this.settings = settings;
  }
  callSlider = () => {
    const slider = this.selector;
    slider.slick(this.settings);
  }
  init = () => {
    this.setSettings();
    this.callSlider();
  }
}
class FeaturedProductCall {
  constructor() {
    if ($(window).width() < 768) {
      return;
    }
    $(".featured-products .slickSlider").each(function (index, item) {
      let slideShowObj = new FeaturedProduct($(item));
      slideShowObj.init();
    })
  }
}

$(document).ready(function () {
  new FeaturedProductCall
  $(document)
    .on('shopify:section:load', function () { new FeaturedProductCall });
})