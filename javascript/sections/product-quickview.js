const { showLoading } = require("../helper");
import ProductJs from './product-page';

class ProductQuickView {
  constructor() {
    this.init();
  }

  createFancyBox = content => {
    $.fancybox.open(content, {
      animationDuration: 300,
      touch:false,
      type : 'iframe',
      afterLoad: function (instance) {
        new ProductJs(content);
        Shopify.PaymentButton.init();
      }
    });
  }

  clickEvent = () => {
    const that = this;
    $(document).on("click", ".product-main .quick-view", function (e) {
      const parent = $(this).closest(".product-main");
      const product = $(this).attr("data-product");
      showLoading();
      const url = "/products/" + product + '?view=quickview';
      fetch(url).then(res => res.text()).then(res => {
        $.fancybox.close();
        $(".parent")
        that.createFancyBox($(res));
      }).catch((error) => {
        $.fancybox.open(`<div class="message">${error}</div>`);
      })

    });
  }
  init = () => {
    this.clickEvent();
  }
}
new ProductQuickView;