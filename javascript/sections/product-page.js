import VariantSelector from '../variant-selector';
class ProductPage {
  constructor(parent, quickview = false) {
    /* Variables setup */
    this.parent = parent;
    this.quickview = quickview;
    this.sliderElement;
    this.productJson = JSON.parse($('.product-json', this.parent).html());

    /* Quick fire functions */
    this.createSlider();
    this.clickEvent();
    this.changeEvent();

    /* Instance of variant selector class */
    this.variantSelector = new VariantSelector({
      parent: this.parent,
      productJson: this.productJson,
      productSwatchAction: this.productSwatchAction,
      productSliderAction: this.productSliderAction
    });

    /* First auto call when page/quickview load */
    this.variantSelector.selectChange();
  }


  /* Product slider */
  createSlider = () => {
    const parent = this.parent;
    const that = this;
    let selector = $(".ProductImages .list", parent);
    let thumbBlock = $(".ProductImages .thumb", parent);
    let thumbLength = $(".single-thumb", parent).length;
    let slidesToShow = 5;
    if (thumbLength == 2) {
      slidesToShow = 2;
    }
    if ($(selector).length > 0 && $(".single-image", selector).length > 1) {
      that.sliderElement = $(selector).slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: thumbBlock,
        dots: true,
        arrows: false
      });
      $(thumbBlock).slick({
        infinite: true,
        slidesToShow: slidesToShow,
        slidesToScroll: 1,
        asNavFor: selector,
        arrows: false,
        focusOnSelect: true
      });
    }
  }
  productSliderAction = index => {
    this.sliderElement.slick('slickGoTo', index);
  }

  /* click event */
  clickEvent = () => {
    const that = this;
    const parent = that.parent;
    $(".swatch-element", parent).click(function () {
      that.productSwatchAction($(this), true);
    });
  }
  /* Product Swatch Action */
  productSwatchAction = (selector, trigger) => {
    selector.siblings().removeClass("active");
    selector.addClass("active");
    if (trigger) {
      let target = selector.attr("data-value");
      let parent = this.parent;
      let swatch = selector.closest('.swatch');
      let oi = swatch.attr("data-option-index");
      oi = parseInt(oi) + 1;
      oi = "option" + oi;
      let select = $(".single-option-selector[data-index='" + oi + "']", parent);
      select.val(target).trigger('change');
    }
  }
  /* change event */
  changeEvent = () => {
    const that = this;
    const parent = that.parent;
    $(".single-option-selector", parent).change(function (event) {
      that.variantSelector.selectChange();
    });
  }
}

/* Auto call for product page */
if ($("#product-page").length > 0) {
  new ProductPage($("#product-page .product-card"));
}
export default ProductPage;