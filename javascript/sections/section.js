simply.homeTestimonials = function () {
  var home_testimonials_images = $(".home_testimonials_text .lightSlider").lightSlider({
    item: 1,
    enableDrag: false,
    loop: true,
    auto: true,
    pause: 7000,
    pager: false,
    mode: 'fade'

  });
};
simply.collectionListSection = function () {
  $("#collection_list ul.list .img").hover(function () {
    $(this).find(".collection_img").addClass('active');
    $(this).find(".text").addClass('active');
  },
    function () {
      $(this).find(".collection_img").removeClass('active');
      $(this).find(".text").removeClass('active');
    });
};
simply.customerAddressSection = function () {
  var $newAddressForm = $('#AddressNewForm');

  if (!$newAddressForm.length) {
    return;
  }
  $('.address-new-toggle').on('click', function () {
    $newAddressForm.toggleClass('hide');
  });
  new Shopify.CountryProvinceSelector('AddressCountryNew', 'AddressProvinceNew', {
    hideElement: 'AddressProvinceContainerNew'
  });

  $('.address-country-option').each(function () {
    var formId = $(this).data('form-id');
    var countrySelector = 'AddressCountry_' + formId;
    var provinceSelector = 'AddressProvince_' + formId;
    var containerSelector = 'AddressProvinceContainer_' + formId;


    new Shopify.CountryProvinceSelector(countrySelector, provinceSelector, {
      hideElement: containerSelector
    });
  });

  $('.address-edit-toggle').on('click', function () {
    var id = $(this).attr("data-form-id");
    var check = $(this).attr("data-toggle");
    $("#address_page .edit_forms").hide();
    $('.address-edit-toggle').not(this).attr("data-toggle", "0");
    if (check == '0') {
      $("#EditAddress_" + id).show();
      $(this).attr("data-toggle", "1");
    }
    else {
      $("#EditAddress_" + id).hide();
      $(this).attr("data-toggle", "0");
    }
  });

  $('.address-delete').on('click', function () {
    var $el = $(this);
    var formId = $el.data('form-id');
    var confirmMessage = $el.data('confirm-message');
    if (confirm(confirmMessage || "Are you sure you wish to delete this address?")) {
      Shopify.postLink('/account/addresses/' + formId, { 'parameters': { '_method': 'delete' } });
    }
  });
};
simply.faqPageSection = function () {
  $("#faq_page .que").click(function () {
    $(this).next(".ans").slideToggle();
    $(this).find("i").toggleClass("fa-angle-down").toggleClass("fa-angle-up");
  });
};
simply.sectionJsInit = function () {
  // 	simply.homeTestimonials();
  simply.collectionListSection();
  simply.customerAddressSection();
  simply.faqPageSection();
};
$(document).ready(function () {
  simply.sectionJsInit();
});