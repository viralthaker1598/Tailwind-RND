import siteNavJs from './siteNav';
import topbar from './topbar';
class header {
  constructor() {
    new siteNavJs();
    if ($("header.main-header").attr("data-fix")) {
      $("#shopify-section-header").addClass("sticky");
    }
    if ($(".topbar.slide").length > 0) {
      new topbar;
    }
  }
}
new header;