class fixedHeader {
  constructor() {
    this.lastScrollTop = 0;
    this.scrollEvent();
  }
  scrollEvent = () => {
    const that = this;
    $(window).scroll(function () {
      /* variables */
      const header = $("header.main-header");
      const height = header.outerHeight();
      const clone = $(".header-clone");
      const fix_height = parseInt(height) + 10;
      const active_height = parseInt(height) + 100;
      const wt = $(window).scrollTop();

      if (wt > fix_height) {
        // set header fix and enable clone div
        header.addClass("fixed");
        clone.css("height", height);
      } else {
        header.removeClass("fixed");
        clone.css("height", 0);
      }
      if (wt > active_height && wt < that.lastScrollTop) {
        //If scroll back to up side
        header.addClass("active");
      } else {
        header.removeClass("active");
      }
      //save the last scroll
      that.lastScrollTop = wt;
    })
  }
}
export default fixedHeader;