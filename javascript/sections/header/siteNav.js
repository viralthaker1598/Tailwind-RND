class siteNavJs {
  constructor() {
    this.menuHover();
    this.menuClick();
  }
  menuClick = () => {
    $(".has-child-click.li>a").click(function (e) {
      e.stopPropagation();
      e.preventDefault();
      let parent = $(this).closest(".has-child-click");
      $(".has-child-click.li .child-menu").not($(".child-menu", parent)).hide();
      $(".child-menu", parent).toggle();
    });
    $(".has-child-click.li .child-menu").click(function (e) {
      e.stopPropagation();
    });
    $(document).click(function (e) {
      e.stopPropagation();
    });
  }
  menuHover = () => {
    $(".has-child.li").hover(function () {
      $(".child-menu", this).stop().fadeIn("fast");
    }, function () {
      $(".child-menu", this).stop().fadeOut("fast");
    })
  }

}
export default siteNavJs;