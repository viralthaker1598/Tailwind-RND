class topbar {
  constructor() {
    //variables
    this.parent = $(".topbar.slide");
    this.animationSpeed = $(".topbar.slide").attr("data-speed");
    this.firstDiv = $('.announcement.loop-1', this.parent);

    this.init();
  }
  init = () => {
    const firstDiv = this.firstDiv;
    //get speed for animation from settings
    let animationSpeed = parseInt(this.animationSpeed);
    firstDiv.addClass('open');
    if (isNaN(animationSpeed)) {
      animationSpeed = 3000;
    }
    else {
      animationSpeed = animationSpeed * 1000;
    }

    //set animation loop
    setInterval(() => {
      this.animationLoop();
    }, animationSpeed);
  }
  animationLoop = function () {
    const parent = this.parent;
    const firstDiv = this.firstDiv;
    const current = $('.announcement.open', parent);
    var nextDiv = current.next();
    current.removeClass("open");
    if (nextDiv.length > 0) {
      //if found next element
      nextDiv.addClass('open');
    }
    else {
      //show first element 
      firstDiv.addClass('open');
    }
  }
}
export default topbar;