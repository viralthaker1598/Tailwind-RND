class SlideShow {
  constructor(selector) {
    this.selector = selector;
    this.settings;
  }

  setSettings = () => {
    const slider = this.selector;
    let speed = parseInt(slider.attr("data-speed")),
      fade = slider.attr("data-mode"),
      adaptiveHeight = cb(slider.attr("data-height"));
    fade === 'fade' ? fade = true : fade = false;
    settings = {
      autoplay: true,
      adaptiveHeight: adaptiveHeight,
      autoplaySpeed: speed,
      dots: true,
      speed: 500,
      draggable: true,
      touchThreshold: 5,
      pauseOnHover: false,
      arrows: false,
      fade: fade
    }
    this.settings = settings;
  }
  setResponsive = () => {
    const slider = this.selector;
    if ($(".responsive", slider).length == 0) {
      return false;
    }
    const sliderParent = slider.closest(".shopify-section");
    let headerHeight = $("#shopify-section-header").outerHeight();
    if ($("#shopify-section-topbar").length > 0) {
      headerHeight = headerHeight + $("#shopify-section-topbar").outerHeight();
    }
    headerHeight = `calc(100vh - ${headerHeight}px)`;
    $(".responsive", sliderParent).css("height", headerHeight);

  }
  callSlider = () => {
    const slider = this.selector;
    const animation = cb(slider.attr("data-animation"));
    animation ? slider.slick(this.settings).slickAnimation() : slider.slick(this.settings);
  }
  animations = () => {
    const slider = this.selector;
    const animation = cb(slider.attr("data-animation"));
    if (!animation) {
      return;
    }
    slider.on('beforeChange', function (event, slick, currentSlideNo, nextSlideNo) {
      let currentSlide = $(".slick-current", slider);
      $(".img", currentSlide).addClass('animate-out');
    });
    slider.on('afterChange', function (event, slick, currentSlideNo, nextSlideNo) {
      let currentSlide = $(".slick-current", slider);
      $('.img', slider).removeClass("animate-out");
    });
  }
  init = () => {
    this.setSettings();
    this.setResponsive();
    this.callSlider();
    this.animations();
  }
}
class slideShowCall {
  constructor() {
    $(".slideshow .slickSlider").each(function (index, item) {
      let slideShowObj = new SlideShow($(item));
      slideShowObj.init();
    })
  }
}
$(document).ready(function () {
  new slideShowCall;
  $(document)
    .on('shopify:section:load', function () { new slideShowCall });
})