simply.clickEvent = function(){  
  $(document).on("click",".payment_icons .li",function(){
    var current = $(this).attr("data-select");
    var payment;
    switch (current) {
      case 'cod':
      payment =  $(".content-box__row[data-gateway-group=manual]");
      break;
      case 'credit_card':
      payment =  $(".content-box__row[data-select-gateway=159186952]");
      break;
      case 'paypal':
      payment =  $(".content-box__row[data-select-gateway=134389000]");
      break;
      default:
    // statements_def
    break;
  }
  var temp_text = $(".temp_text");
  temp_text.hide();
  if(!cn(payment)){
    $(".payment_icons .li").removeClass('active');
    $(this).addClass("active");
    $("input",payment).click();
    var text = payment.next(".content-box__row--secondary").find(".blank-slate p").text();
    $(".temp_text").text(text);
    temp_text.show();
  }
});
  $(document).on("click","#shopify-section-checkout-summary .conti .btn",function(){
    $(".step__footer .step__footer__continue-btn").click();
  }); 
}; 
simply.changeEvent = function(){
};
simply.getSummary = function(){
  $(".checkout_summary .content").append($("#order-summary"));
}; 
simply.setEditForm = function(parent){

  var temp_content = parent.html(); 
  parent.html("");
  $(".step .edit_checkout").each(function(index, el) {
    if($(el).closest('.hidden-on-desktop').length == 0){
      parent.append($(el));
    }
  });
  $(".edit_checkout",parent).append(temp_content);
};
simply.editFormFooter = function(parent,custom_parent){
  $(".section_footer",custom_parent).append($(".step__footer",parent));
  $(".section_footer button[type=submit]",custom_parent).addClass("v_hide");
};
simply.contactDesign = function(){
  $("#shopify-section-checkout-shipping").remove();
  $("#shopify-section-checkout-payment").remove();
  $(".steps .ci").addClass('active');
  $(".customer-information.step").addClass('active');
  var parent = $("#shopify-section-checkout-contact");
  simply.setEditForm(parent);
  var custom_parent = $(".checkout_contact",parent);
  $(".news_form",custom_parent).append($(".section--contact-information",parent));
  $(".news_form",custom_parent).append($("p.layout-flex__item",parent));
  $(".main_form",custom_parent).append($(".section--shipping-address",parent));
  $(".main_form",custom_parent).append($(".section--optional",parent)); 
  simply.editFormFooter(parent,custom_parent);
};
simply.shippingDesign = function(){
  $("#shopify-section-checkout-payment").remove();
  $("#shopify-section-checkout-contact").remove();
  $(".steps .sm").addClass('active');
  $(".shipping-method.step").addClass('active');
  $("#shopify-section-checkout-summary .conti .btn").text("CONTINUE TO PAYMENT");
  var parent = $("#shopify-section-checkout-shipping");
  simply.setEditForm(parent);
  var custom_parent = $(".checkout_shipping",parent);
  $(".contact_info",parent).append($(".step__sections",parent));
  $(".main_form",parent).append($(".section--shipping-method",parent));
  simply.editFormFooter(parent,custom_parent);
};
simply.paymentDesign = function(){
  $("#shopify-section-checkout-shipping").remove();
  $("#shopify-section-checkout-contact").remove();
  $(".steps .pm").addClass('active');
  $(".payment-method.step").addClass('active');
  $("#shopify-section-checkout-summary .conti .btn").text("COMPLETE ORDER");
  var parent = $("#shopify-section-checkout-payment");
  simply.setEditForm(parent);
  var custom_parent = $(".checkout_payment",parent);
  $(".payment_content",custom_parent).append($(".section--payment-method",parent));
  $(".billing_content",custom_parent).append($(".section--billing-address",parent));
  simply.editFormFooter(parent,custom_parent);
  $(".payment_icons .li:first").click();
  $(".content-box__row[data-same-billing-address] input").click();
};
simply.bread = function(){
  var bread = $(".breadcrumb:first");
  var count = 0;
  $(".breadcrumb__item",bread).each(function(index, el) {
    if(bread.children().length == 4 && index == 0){
    }
    else{
      var link = $("a",el).attr("href");
      if(!cn(link)){
        $(".steps .li:nth("+count+") a").attr("href",link);  
      }
      count++;
    }
  });
};
simply.init = function(){
if($(window).width() > 999){
  switch (Shopify.Checkout.step) {
    case 'contact_information':
    simply.getSummary();
    simply.contactDesign();
    simply.bread();
    break;
    case 'shipping_method':
    simply.getSummary();
    simply.shippingDesign();
    simply.bread();
    break;
    case 'payment_method':
    simply.getSummary();
    simply.paymentDesign();
    simply.bread();
    break;

    default:
    $(".main_content").removeClass("main_content");
    $(".custom_checkout").addClass("hide_important");
    break;
  }
  $(".custom_checkout").fadeIn();
}
else{
  $(".custom_checkout").addClass("hide_important");
}

}; 
$(document).ready(function(){
  window.locs;
  window.current_loc;
  simply.clickEvent();
  simply.changeEvent();
  simply.init();
}); 