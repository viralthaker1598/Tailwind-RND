window.$ = window.jQuery = $;
/*================ Vendors ================*/
require('./vendor/lazy-load');
require('./vendor/slick-slider');
require('@fancyapps/fancybox');

/*================ Snippets ================*/
require('./snippets/common');
require('./snippets/mobile-menu');
require('./snippets/multi-currency');
require('./snippets/search');
require('./snippets/newsletter-popup');

/*================ Sections ================*/
require('./sections/header/header.js');
require('./sections/slideshow');
require('./sections/featured-products');
require('./sections/product-page');
require('./sections/product-recommendations');
require('./sections/product-quickview');

import section from './sections/section';
import simply from '../javascript/simply.js';
import collectionsort from '../javascript/collection-sort.js';
import collectionFilter from '../javascript/filter-script.js';
import imageZoom from '../javascript/image-zoom.js';
import scss from '../scss/app.scss';

