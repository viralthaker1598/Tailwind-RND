import { formatMoney } from "./helper";

class VariantSelector {
  constructor(obj) {
    this.parent = obj.parent;
    this.productJson = obj.productJson;
    this.productSwatchAction = obj.productSwatchAction;
    this.productSliderAction = obj.productSliderAction;
  };

  selectChange = () => {
    let productCard = this.parent;
    if (!cn(this.productJson)) {
      let values = [];
      $(".single-option-selector", productCard).each(function (index, el) {
        let obj = {};
        obj.index = $(el).attr("data-index");
        obj.value = $(el).val();
        values.push(obj);
      });
      let variant = this.findVariant(values);
      if (variant) {
        $(".single-variants", productCard).val(variant.id);
        this.queryString(variant.id);
        this.variant = variant;
        this.productCallback();
      }
    }
  }
  findVariant(values) {
    let variants = this.productJson.variants;
    let size = values.length;
    for (let i = 0; i < variants.length; i++) {
      let variant = variants[i];
      if (size == 1) {
        if (variant[values[0].index] == values[0].value) {
          return variant;
        }
      }
      else if (size == 2) {
        if (variant[values[0].index] == values[0].value && variant[values[1].index] == values[1].value) {
          return variant;
        }
      }
      else {
        if (variant[values[0].index] == values[0].value && variant[values[1].index] == values[1].value && variant[values[2].index] == values[2].value) {
          return variant;
        }
      }
    }
  }
  productCallback() {
    const that = this;
    let variant = that.variant,
      parent = that.parent,
      translations = simply.translations;
    let $addToCart = $(".AddToCart", parent),
      $sku = $(".product-sku", parent),
      $productPrice = $('.ProductPrice', parent),
      $comparePrice = $('.ComparePrice', parent),
      $quantityElements = $('.js-quantity-selector, label + .js-qty', parent),
      $productImg = $(".ProductImages", parent),
      $addToCartText = $('.AddToCartText', parent);

    if (variant) {
      if (variant.featured_image) {
        let imgObj = variant.featured_image;
        let new_img = imgObj.id;
        new_img = ".id_" + new_img;
        let index = $productImg.find(new_img).attr("data-index");
        if (!cn(index) && that.productSliderAction) {
          index = parseInt(index) - 1;
          that.productSliderAction(index);
        }
      }
      for (let i = 0, length = variant.options.length; i < length; i++) {
        let option = variant.options[i];
        let swatch = $('.swatch-element[data-value="' + option + '"]', parent);
        that.productSwatchAction(swatch, false);
      }
      if (variant.available) {
        $addToCart.removeClass('disabled').prop('disabled', false);
        $addToCartText.html(translations.addToCart);
        $quantityElements.show();
        $quantityElements.attr("disabled", false);
      } else {
        $addToCart.addClass('disabled').prop('disabled', true);
        $addToCartText.html(translations.soldOut);
        $quantityElements.attr("disabled", true);
      }
      $productPrice.html(formatMoney(variant.price).replace(/((\,00)|(\.00))$/g, '')).show();
      if (variant.compare_at_price > variant.price) {
        $comparePrice.html(formatMoney(variant.compare_at_price).replace(/((\,00)|(\.00))$/g, ''));
        $productPrice.addClass('on-sale');
      } else {
        $productPrice.removeClass('on-sale');
        $comparePrice.html("");
      }
      $sku.html(variant.sku);
    } else {
      $addToCart.addClass('disabled').prop('disabled', true);
      $comparePrice.html("");
      $addToCartText.html(translations.unavailable);
      $quantityElements.attr("disabled", true);
    }

  }
  queryString(value) {
    let key = "variant";
    let uri = window.location.href;
    let re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    let separator = uri.indexOf('?') !== -1 ? "&" : "?";
    let newUrl = "";
    if (uri.match(re)) {
      newUrl = uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
      newUrl = uri + separator + key + "=" + value;
    }
    window.history.replaceState({}, document.title, newUrl);
  }
}


export default VariantSelector;