/* Shopify Money Format */
export const formatMoney = function (t, e = simply.money_format) {
  function o(t, e) {
    return void 0 === t ? e : t
  }
  function i(t, e, i, r) {
    if (e = o(e, 2),
      i = o(i, ","),
      r = o(r, "."),
      isNaN(t) || null == t)
      return 0;
    t = (t / 100).toFixed(e);
    var n = t.split(".");
    return n[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1" + i) + (n[1] ? r + n[1] : "")
  }
  "string" == typeof t && (t = t.replace(".", ""));
  var r = ""
    , n = /\{\{\s*(\w+)\s*\}\}/
    , a = e || this.money_format;
  switch (a.match(n)[1]) {
    case "amount":
      r = i(t, 2);
      break;
    case "amount_no_decimals":
      r = i(t, 0);
      break;
    case "amount_with_comma_separator":
      r = i(t, 2, ".", ",");
      break;
    case "amount_with_space_separator":
      r = i(t, 2, " ", ",");
      break;
    case "amount_with_period_and_space_separator":
      r = i(t, 2, " ", ".");
      break;
    case "amount_no_decimals_with_comma_separator":
      r = i(t, 0, ".", ",");
      break;
    case "amount_no_decimals_with_space_separator":
      r = i(t, 0, " ");
      break;
    case "amount_with_apostrophe_separator":
      r = i(t, 2, "'", ".")
  }
  return a.replace(n, r)
};

/* to show/hide black black background */
export const blackBgOpen = () => {
  $(".black-bg").fadeIn();
  $("html").addClass("overflow-hidden");
  $("body").addClass("overflow-hidden");
}
export const blackBgClose = () => {
  $(".black-bg").fadeOut();
  $("html").removeClass("overflow-hidden");
  $("body").removeClass("overflow-hidden");
}

export const showLoading = () => {
  $.fancybox.open(`<div class="fancybox-loading"></div>`, {
    hideScrollbar: false,
    btnTpl: {
      smallBtn: ''
    }
  });
};
export const setCookie = (cname, cValue, exDays) => {
  var d = new Date();
  d.setTime(d.getTime() + (exDays * 24 * 60 * 60 * 1000));
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cValue + ";" + expires + ";path=/";
};

export const getCookie = cname => {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
};