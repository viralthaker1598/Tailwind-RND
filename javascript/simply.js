/**
 *  Section Plus Shopify Theme v1.0.0
 *  
 *  Created on : 6 Aug, 2016, 3:05:23 PM
 *  
 *  Support - shopify@lucentinnovation.com
 *  
 *  Copyright 2016-2017 Lucent Innovation
 */


// Events
window.$ = window.jQuery = $;
simply.clickEvent = function() { //click
    //document on click
    //search 
    $(".search_clear").click(function(event) {
      $(".fake_search .input-box").val("");
      $(".quick_search .msg").html("");
    });
    $(".searchInit").click(function(event) {
      simply.searchOpen();
    });
    $(".search_close").click(function(event) {
      simply.searchClose();
    });
    //spinner
    $(document).on("click", ".spinner .plus", function() {
      var target = $(this).siblings("input");
      var value = parseInt(target.val()) + 1;
      target.val(value).trigger('change');
    });
    $(document).on("click", ".spinner .min", function() {
      var target = $(this).siblings("input");
      var value = parseInt(target.val());
      if (value > 0) {
        var value = value - 1;
        target.val(value).trigger('change');
      }
    });
    //quick view


    //recover password
    $("#RecoverPassword").click(simply.toggleRecoverPasswordForm);
    $("#HideRecoverPasswordLink").click(simply.toggleRecoverPasswordForm);

    //mini cart open
    $(document).on("click", ".side-cart", function() {
      simply.miniCartOpen();
    });
    //   filter open
    $("#collection_page .filter_btn").click(function() {
      simply.filterOpen();
    });
    // mobile menu, mini cart and filter close
    $(document).on("click", ".black-bg", function() {
      simply.closeAllDrawer();
    });
    $(document).on("click", ".body-wrapper.active", function() {
      simply.closeAllDrawer();
    });
    $(document).on("click", "#side-cart .close", function() { //mini cart close
      simply.miniCartClose();
    });
    $(".advance_filter .filter_title .close").click(function() { //filter close
      simply.filterClose();
    });

    //mini cart remove product
    $(document).on("click", "#side-cart .remove", function() {
      $(this).text("Removing...");
      var input = $(this).parent().find("input");
      input.val(0);
      var form = $("#side-cart form");
      var remove = $(this);
      simply.miniCartUpdate(form);
    });

    //mobile search
    $(".mobile-search-icon .search").click(function() {
      $(".mobile-search").removeClass("hide");
      $(".mobile-search input").attr("autofocus", true);
    });
    $(".mobile-search .close").click(function() {
      $(".mobile-search").addClass("hide");
    });


    //collection swatches
    $(document).on("click", "#collection_page .color.swatch-element", function() {
      simply.collectionSwatchesChange($(this));
    });
  };

simply.hoverEvent = function() { //hover

    $(document).on("mouseenter", ".product-main .product-img", function() {
      if (simply.current_width > 1024) {
            var next = $(this).next(); //image switch
            if (next.hasClass('alter_img')) {
              $(this).stop().css("opacity", "0");
              $(this).next(".alter_img").show();
            }
          }
        });
    $(document).on("mouseleave", ".product-main .product-img", function() {
      if (simply.current_width > 1024) {
            var next = $(this).next(); //image switch
            if (next.hasClass('alter_img')) {
              $(this).stop().css("opacity", "1");
              $(this).next(".alter_img").hide();
            }
          }
        });

    $(document).on("mouseenter", ".product-main", function() {
        $(this).find(".quick-view").stop().fadeIn(); //quick view banner 
      });
    $(document).on("mouseleave", ".product-main", function() {
        $(this).find(".quick-view").stop().fadeOut(); //quick view banner 
      });
  };

  simply.submitEvent = function() {

    //ajax cart
    $(document).on("submit", ".ajaxCart", function(e) {
      e.preventDefault();
      var $form = $(this);
      var button = $(".AddToCartText", $form);
      button.text(button.attr("data-process"));
      simply.ajaxCart($form, button);
    });
  };

simply.changeEvent = function() { // search ajax
  simply.lastSearch;
  $(".quick_search .input-box").on("change paste keyup", function() {
    var that = $(this);
    setTimeout(function() {
      console.log("search called");
      var parent = that.closest(".quick_search");
      var search_result = $(".search_result", parent);
      var loading = $(".search_loading", parent);
      var search_data = $(".search_data", parent);
      var msg = $(".msg", parent);
      var value = that.val();
      var q = that.val().length;
      if (q >= 3 && simply.lastSearch != value) {
        loading.show();
        $.get("/search?view=ajax&q=" + value, function(data, status) {
          simply.lastSearch = value;
          loading.hide();
          var data = $(data);
          var matches = data.children().length;
          if (matches == 0) {
            search_data.hide();
            msg.html("Data not found for " + simply.lastSearch + ".");
          } else {
            msg.html("Found " + matches + " matches for " + simply.lastSearch + ".");
            search_data.fadeIn();
            search_data.html(data);
          }
          msg.show();
        });
      }
    }, 1000);
  });

  $(document).on("change", "#side-cart .qty.cart_item input", function() {
    var form = $(this).closest('form');
    setTimeout(function() {
      simply.miniCartUpdate(form);
    }, 1000);
  });
};

simply.focusEvent = function() {

  };
// end of events
simply.searchOpen = function() {
  $(".quick_search").addClass('active');
  $("html,body").addClass('overflow-hidden');
  $(".quick_search .input-box").focus();
};
simply.searchClose = function() {
  $(".quick_search").removeClass('active');
  $("html,body").removeClass('overflow-hidden');
};
simply.formFocus = function(selector) {
  selector.addClass("active");
  if ($(selector).is(':checkbox')) {
    selector.siblings("label").removeClass("active");
  } else {
    selector.siblings("label").addClass("active");
  }
  selector.siblings(".prefix").addClass("active");
};
simply.formFocusOut = function(selector) {
  var value = selector.val();
  if (cn(value)) {
    selector.siblings("label").removeClass("active");
    selector.siblings(".prefix").removeClass("active");
    selector.removeClass("active");
  } else {
    if ($(selector).is(':checkbox')) {
      selector.siblings("label").removeClass("active");
    } else {
      selector.siblings("label").addClass("active");
    }
    selector.siblings(".prefix").addClass("active");
    selector.addClass("active");
  }
};

simply.queryString = function() {
  var vars = {};
  var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
  for (var i = 0; i < hashes.length; i++) {
    var hash = hashes[i].split('=');
    var obj = {};
    obj.name = decodeURI(hash[0]);
    obj.value = decodeURI(hash[1]);
    vars[obj.name] = obj;
  }
  return vars;
};
simply.mobileQuickViewOpen = function() {
  if (simply.current_width < 768) {
    $("#quick_view").addClass('open');
    $("html").addClass('overflow-hidden');
    $("body").addClass('overflow-hidden');
  }
};
simply.mobileQuickViewClose = function() {
  if (simply.current_width < 768) {
    $("#quick_view").removeClass('open');
    $("html").removeClass('overflow-hidden');
    $("body").removeClass('overflow-hidden');
  }
};

simply.ajaxCart = function(form, success_call,error_call) {
  var data = form.serialize();
  var url = form.attr("action");
  var params = {
    type: 'POST',
    url: url,
    data: data,
    dataType: 'json',
    success: function(line_item) {
      if(!cn(success_call))
        success_call(line_item);
    },
    error: function(XMLHttpRequest, textStatus) {
      var error = JSON.parse((XMLHttpRequest.responseText)).description;
      if(!cn(error_call))
       error_call(error);
   }
 };
 jQuery.ajax(params);
};
simply.ajaxCart = function(form, button) {
  var data = form.serialize();
  var params = {
    type: 'POST',
    url: '/cart/add.js',
    data: data,
    dataType: 'json',
    success: function(line_item) {
      simply.updateCartCount(line_item.item_count);
      if (simply.sideCartCheck == 0) {
        simply.mobileQuickViewClose();
        var result = $("#quick-add-result");
        $.fancybox(result);
      } else {
        simply.miniCartInit("open");
      }
      button.text(button.attr("data-text"));
    },
    error: function(XMLHttpRequest, textStatus) {
      var error = JSON.parse((XMLHttpRequest.responseText)).description;
      var error_popup = $("<div id='quick-error'></div>");
      error_popup.html("");
      error_popup.append("<h6>" + error + "</h6>");
      $.fancybox.open(error_popup);
      button.text(button.attr("data-text"));
    }
  };
  jQuery.ajax(params);
};
simply.updateCartCount = function(qty) {
  var cart = $(".cart .count:first");
  if (cart.length > 0) {
    var cart_value = cart.text();
    cart_value = parseInt(cart_value) + parseInt(qty);
    $(".cart .count").html(cart_value);
  }
};
simply.miniCartInit = function(open) {
  if (simply.sideCartCheck != 0) {
    var cart;
    var url = "/cart?view=sidecart";
    $.ajax({
      url: url,
      success: function(cartpage) {
        if ($("#side-cart").length > 0) {
          $("#side-cart").html("");
          cartpage = $(cartpage).filter("#side-cart").html();
          $("#side-cart").append(cartpage);
          var count = $(".count.hide").text();
          $(".cart .count").html(count);
        } 
        if (!cn(open)) {
          $.fancybox.close();
          simply.miniCartOpen();
        }
      }
    });
  }
};
simply.miniCartUpdate = function(form) {
  $(".mini_cart_loading").fadeIn();
  var params = {
    type: 'POST',
    url: '/cart',
    data: form.serialize(),
    dataType: 'json',
    success: function(line_item) {
      var cart_count = line_item.item_count;
      $(".cart .count").html(cart_count);
      $(".mini_cart_loading").hide();
      simply.miniCartInit();
    },
    error: function(XMLHttpRequest, textStatus) {
      var error = JSON.parse((XMLHttpRequest.responseText)).description;
      error = "<h6>" + error + "</h6>";
      $("#fancy-msg").html("").append(error);
      $.fancybox.open("#fancy-msg");
      $(".mini_cart_loading").hide();
    }
  };
  $("#side-cart .no_data").fadeOut();
  jQuery.ajax(params);
};
simply.filterOpen = function() {
  if ($(".filter_btn").length > 0) {
    simply.miniCartClose();
    $(".fixed_filter").addClass("active");
    simply.blackBgOpen();
  }
};
simply.filterClose = function() {
  if ($(".filter_btn").length > 0) {
    $(".fixed_filter").removeClass("active");
    simply.blackBgClose();
  }
};
simply.miniCartOpen = function() {
  $("#side-cart").addClass("active");
  $(".body-wrapper").addClass("active-right active");
  simply.blackBgOpen();
  simply.mobileQuickViewClose();
};
simply.miniCartClose = function() {
  $("#side-cart").removeClass("active");
  $(".body-wrapper").removeClass("active-right active");
  simply.blackBgClose();
};
simply.blackBgOpen = function() {
  $(".black-bg").fadeIn();
  $("html").addClass("overflow-hidden");
  $("body").addClass("overflow-hidden");
};
simply.blackBgClose = function() {
  $(".black-bg").fadeOut();
  $("html").removeClass("overflow-hidden");
  $("body").removeClass("overflow-hidden");
};


simply.initMobileMenu = function() {
  var data = $("#mobile-drawer");
  $("body").append(data);
};

simply.miniHeightAction = function(selector) {
  var min_height = 0;
  selector.each(function() {
    var height = $(this).outerHeight();
    if (height > min_height) {
      min_height = height;
    }
  });
  return min_height;
};
simply.miniHeight = function(parent, select) {


  var parentWrapper = $(parent).closest(".shopify-section");
  parentWrapper.each(function(index, item) {
    if ($(parent, item).length > 0) {
      var result = simply.miniHeightAction($(select, item));
      $(parent, item).css("height", result);
    }
  });

};
simply.productZoom = function() {
  var images = ".product-card .image-block .ProductImages .zoom";
  if (simply.current_width > 1024) {
    if ($(images).length > 0) {
      $(images).zoom();
    }
  }
};

simply.checkUrlHash = function() {
  var hash = simply.getHash();

    // Allow deep linking to recover password form
    if (hash == '#recover') {
      simply.toggleRecoverPasswordForm();
    }
  };
  simply.toggleRecoverPasswordForm = function() {
    $('#RecoverPasswordForm').toggleClass('hide');
    $('#CustomerLoginForm').toggleClass('hide');
  };
  simply.getHash = function() {
    return window.location.hash;
  };
  simply.resetPassword = function() {
    if ($("#login_page").length > 0) {
      if ($(".reset-password-success").length > 0) {
        $("#ResetSuccess").removeClass("hide");
      }
    }
  };


  simply.collectionTopbar = function(bt) {

    var topbar = $("#collection_page .collection_top");
    var clone = $("#collection_page .filter_clone");
    var th = topbar.outerHeight(); //collection topbar height
    bt = parseInt(th) + bt;
    var wt = $(window).scrollTop(); //wt = window top
    var top_postion = 0;
    var show_filter = bt + 100;
    if ($(".main-header.fixed").length > 0) {
      var header = $(".main-header.fixed").outerHeight();
      wt = parseInt(wt) + parseInt(header);
      top_postion = header;
    }
    if (wt > bt) {
      clone.css("height", th);
      topbar.addClass("fixed");

      if (topbar.find(".container").length == 0) {
        topbar.find(".filter_tape").wrap("<div class='container'></div>");
      }
    } else {
      clone.css("height", 0);
      topbar.removeClass("fixed");
      if (topbar.find(".container").length > 0) {
        topbar.find(".filter_tape").unwrap();
      }
    }
    if (wt > show_filter) {
      topbar.css("top", top_postion);
    } else {
      topbar.css("top", '-100%');
    }
  };
  simply.productSelectHide = function() {
    if ($(".product-card .swatches").length > 0) {
      $(".product-card .swatches .swatch").each(function(index, item) {
        var target = $(item).attr("data-option-index");
        target = parseInt(target) + 1;
        target = "option" + target;
        $(".single-option-selector[data-option='" + target + "']").parent().addClass("hide");
      });
    }
  };
  simply.collectionSwatchesChange = function(selector) {
    var parent_id = selector.parent().attr("data-id");
    var parent = $(".product-main." + parent_id);
    var loading = parent.siblings(".loading");
    var current_img = parent.find(".product-img");
    var new_img = selector.find("label").attr("data-img");
    selector.siblings().removeClass("active");
    selector.addClass("active");
    if (!cn(new_img)) {
      parent.addClass("disabled");
      loading.show();
      var image = new Image();
      image.onload = function() {
        current_img.attr("src", new_img).attr("srcset", new_img);
        loading.hide();
        parent.removeClass("disabled");
        //simply.miniHeight(simply.productImgParent, simply.productImgChild);
      };
      image.src = new_img;
    }
  };
  simply.productSwatchesInit = function(parent) {
    if ($(".product-card .swatches").length > 0) {
      parent.find(".single-option-selector").each(function(index, item) {
        var value = $(item).val();
        var target = $(item).attr("data-option");
        target = target.replace("option", "");
        target = parseInt(target) - 1;
        var select = parent.find(".swatch[data-option-index='" + target + "']");
        select.find(".swatch-element[data-value='" + value + "']").addClass("active");
      });

      parent.find(".swatches").each(function() {
        if ($(this).find(".swatch-element").length < 2) {
          $(this).hide();
        }
      });

    }
  };

  simply.sliderCallback = function(parent) {
    $(parent + " .v_hide").removeClass("v_hide");
  };

  
  simply.shuffle = function(array) {
    var currentIndex = array.length,
    temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
      }

      return array;
    };
    simply.recommendedFetch = function(collections, plength) {
      var recommended_block = $(".recommended_products");
      var width_class = recommended_block.attr('data-width');
      var productPerCollection = parseInt(recommended_block.attr('data-products'));
      var productPerCollection = Math.ceil(productPerCollection / parseInt(plength));
      if (collections.length > 0) {
        var collection = collections.pop();
        collection = '/collections/' + collection + "?view=recent&sort_by=created-descending";
        $.ajax({
          url: collection,
          success: function(data) {
            var data = $(data);
            var products = data.filter(".product-main");
            products = simply.shuffle(products);
            var limit;
            if (products.length < productPerCollection) {
              limit = products.length;
            } else {
              limit = productPerCollection;
            }
            for (var i = 0; i < limit; i++) {
              var li = $('<li class="' + width_class + '">');
              li.append(products[i]);
              $(".list", recommended_block).append(li);
            }
            simply.recommendedFetch(collections, plength);
            recommended_block.show();
          }
        });
      }

    };
  
    simply.init = function() {
      simply.initMobileMenu();
      simply.clickEvent();
      simply.hoverEvent();
      simply.submitEvent();
      simply.changeEvent();
      simply.focusEvent();
      if ($("#product-page").length > 0) {
        simply.productZoom();
      }
      simply.checkUrlHash();
      simply.resetPassword();
      simply.miniCartInit();
      $(".back_button").attr("href", document.referrer);
    };
    simply.closeAllDrawer = function() {
      simply.miniCartClose();
      simply.filterClose();
    };
  
  simply.scroll = function() {
    if ($("#collection_page .collection_top").length > 0) {
      var collection_topbar = $("#collection_page .collection_top").offset().top;
    }
    $(window).scroll(function() {
      if ($("#collection_page .collection_top").length > 0) {
        simply.collectionTopbar(collection_topbar);
      }
    });
  };
  $(document).ready(function() {
    // variables
    simply.current_width = $(window).width();
    simply.product_page_slider = '';
    simply.lastScrollTop = 0;
    simply.sideCartCheck = $(".side-cart").length;
    simply.srcArray = [480, 765, 900, 1000, 1100, 1300, 1500, 1700];
    simply.productImgParent = ".product-main .image";
    simply.productImgChild = ".product-main .image .img>.product-link";
    //   end of variables
    simply.init();
    simply.scroll();
  });