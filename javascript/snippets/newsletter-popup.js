import { getCookie, setCookie } from '../helper';
class NewsletterPopup {
  constructor() {
    this.selector = $("#welcome-popup");
    if (this.selector.length === 0 || window.location.href.includes('challenge')) {
      return;
    }
    this.welcomeCookieExpire = this.selector.attr("data-day");
    this.timerTime = Number(this.selector.attr("data-time")) * 1000;
    if (window.location.href.includes("customer_posted=true")) {
      this.timerTime = 0;
    }
    //  Test mode
    if (this.selector.attr("data-test-mode")) {
      this.timerTime = 0;
      this.init();
      return;
    }

    //  Regular mode
    const user = getCookie("subscriber");
    if (!user) {
      this.init();
    }
  }
  init = () => {
    const that = this;
    setTimeout(() => {
      $.fancybox.open(that.selector, {
        afterClose: function () {
          setCookie("subscriber", "user", that.welcomeCookieExpire);
        },
        beforeClose: function (instance, current, e) {
          if ($(e.currentTarget).parent(".fancybox-button").length === 0) {
            if ($(e.currentTarget).hasClass("fancybox-button")) {
              return true;
            }
            return false;
          }
        }
      });
    }, that.timerTime);
  }
}

new NewsletterPopup;