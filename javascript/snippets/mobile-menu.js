import { blackBgClose, blackBgOpen } from '../helper';
class mobileMenu {
  constructor() {
    this.clickEvent();
  }
  clickEvent = () => {
    const that = this;
    /* open mobile menu */
    $(".mobile-menu-btn").click(function (e) {
      e.stopPropagation();
      that.openMobileMenu();
    });
    /* close mobile menu */
    $("#mobile-drawer .drawer-close").click(function () {
      that.closeMobileMenu();
    });
    /* open second submenu */
    $(document).on("click", "#mobile-drawer .child-li", function (e) {
      e.stopPropagation();
      var li = $(this);
      $(".link-data", li).slideToggle();
    });
    /* open thid submenu */
    $(document).on("click", "#mobile-drawer .li .icon-wrap", function (e) {
      e.stopPropagation();
      $("span", $(this)).toggleClass("hide");
      $(this).parent().next(".child-menu").slideToggle();
    });
    /* to prevent anchor tag click */
    $(document).on("click", "#mobile-drawer .li a", function (e) {
      e.stopPropagation();
    });
  }

  openMobileMenu = () => {
    $("#mobile-drawer").addClass("active");
    blackBgOpen();
  }

  closeMobileMenu = () => {
    $("#mobile-drawer").removeClass("active");
    blackBgClose();
  }

}
new mobileMenu;