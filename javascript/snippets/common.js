
/* Common click event to hide elements on document click */
$(document).click(function () {
  $(".main-header .search-form").hide();// search box
  $(".has-child-click.li .child-menu").hide();// desktop submenu - If only it opens on click
});

/* Resize trigger */
simply.resizeTrigger = function () {
  window.dispatchEvent(new Event('resize'));
};