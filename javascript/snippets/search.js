import { formatMoney } from '../helper';
class searchApi {
  constructor() {
    this.clickEvent();
    this.changeEvent();
    this.submitEvent();
    this.parent;
    this.loading;
  }
  submitEvent = () => {
    /* To prevent enther in mobile phone */
    $(document).on("submit", ".quick-search", function (e) {
      e.preventDefault();
    });
  }
  clickEvent = () => {
    const that = this;
    /* Desktop searchbox open/close */
    $(".main-header .search-button").click(function (e) {
      e.stopPropagation();
      $(".main-header .search-form").toggle();
      $(".main-header .search-form .input-box").focus();
    });

    /* to prevent hide when click on input */
    $(".search-form").click(function (e) {
      e.stopPropagation();
    });

    /* close search-box */
    $(".main-header .close-form").click(function () {
      $("html,body").removeClass("overflow-hidden");
      $(this).closest(".search-form").hide();
      that.resetSearchBox();
    })

    /* Mobile/Tablet search open */
    $(document).on("click", ".mobile-search", function (e) {
      e.stopPropagation();
      $("html,body").addClass("overflow-hidden");
      if ($(window).width() < 1025 && $(".quick-search.moved").length == 0) {
        /* Append to body at first click because of fix position */
        $(".quick-search").addClass("moved").appendTo($("body"));
      }
      $(".quick-search").show();
      $(".quick-search .input-box").focus();
    });
  }
  changeEvent = () => {
    const that = this;
    /* search after three letters typed */
    let lastSearch;
    $(".quick-search .input-box").on("change paste keyup", function () {
      let selector = $(this);
      setTimeout(function () {
        that.parent = selector.closest(".quick-search");
        var search_container = $(".search-result", that.parent);
        that.loading = $(".search-loading", that.parent);
        var term = selector.val();
        var q = selector.val().length;
        if (q >= 3 && lastSearch != term) {
          lastSearch = term;/* store search result to prevent fire query for same search */
          search_container.show();
          that.loading.show();
          that.performSearch(term);
        }
      }, 1000);
    });

  }
  /* Shopify predictive search API Callback */
  searchCallback = (status, searchData) => {
    const parent = this.parent;
    const loading = this.loading;
    const msg = $(".msg", parent);
    if (!status) {
      msg.text("Something went wrong");
      loading.hide();
    }
    try {
      const products = searchData.resources.results.products;
      let rHtml = "";
      msg.text("");
      if (products.length == 0) {
        msg.text("No Result Found");
      }
      else {
        msg.text(`${products.length} Results Found`);
      }
      products.map(product => {
        rHtml += `<div class='product flex-view-xs'>
          <div class="img col-xs-4 col-sm-2">
            <a href="${product.url}">
              <img  src='${product.featured_image.url}' alt='${product.featured_image.alt}'/>
            </a>
          </div>
          <div class='text col-xs-8 col-sm-10'>
            <a href="${product.url}">
              <p>${product.title}</p>
            </a>
            <p class='price'>${formatMoney(parseFloat(product.price) * 100)}</p>
          </div>
        </div>`;
        return product;
      })
      loading.hide();
      $(".search-data", parent).html(rHtml);
    }
    catch (e) {
      console.log(e.message);
    }
  }
  /* Shopify predictive search API Call */
  performSearch = async (term) => {
    const url = `/search/suggest.json?q=${term}&resources[type]=product`;
    let searchResponse;
    try {
      searchResponse = await fetch(url);
      searchResponse = await searchResponse.json();
      this.searchCallback(true, searchResponse);
    } catch (e) {
      this.searchCallback(false, []);
    }
  }
  /* Reset searchbox on close button click */
  resetSearchBox = () => {
    $(".search-result").hide();
    $(".quick-search .input-box").val("");
  }
}

new searchApi;